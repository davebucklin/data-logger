#!/usr/bin/perl
# Why yes, I have read Modern Perl.
use 5.020; # implies use strict;
use warnings;
use autodie;
# Term::RawInput decodes keypresses.
# https://metacpan.org/pod/Term::RawInput
use Term::RawInput;
use POSIX qw(strftime);
#
# Tree structre method stolen from perlmonks.
#
open my $fh, '<', 'options.txt' or die "Cannot read options.txt.\n";
my @stack;
my %hash;
push @stack, \%hash;
while(<$fh>){
  chomp;
  s/^(\t*)//;
  splice @stack, length($1)+1;
  push @stack, $stack[$#stack]->{$_} = {};
}
close $fh;
# reference stack, the path through the tree.
my @refstack;
# human-readable trail of key values for logging.
my @path;
# the current list of options.
my @options;
# $ptr stores our position in @options.
my $ptr;
#
# $in will store text input that we get from rawInput().  I actually
# don't use $in at all.  $key stores the name of the key that was
# pressed while in rawInput().  I only care about using the arrow keys
# to traverse the menu and select the current option.
#
my ($in,$key) = ('','');
push @refstack, \%hash;
$ptr = 0;
@options = keys %{$refstack[$#refstack]};

do {
  # Capture a key from the terminal.  It would be more interesting
  # to capture keyboard input without needing a shell to be logged
  # in. This would mean getting input from something like /dev/hidraw0
  # or some device that picks up keystrokes from an attached
  # keyboard. Alternatively, these actions could come from the GPIO.
  print $options[$ptr], "\n";
  ($in,$key) = rawInput('',0);

  # Take appropriate action based on the incoming $key
  #
  # For up and down, we just nudge the pointer to the current list
  # of options.
  #
  if ( $key eq "UPARROW") {
    $ptr --;
    if ($ptr < 0) {
      $ptr = $#options;
    }
  }
  if ( $key eq "DOWNARROW" ) {
    $ptr ++;
    if ( $ptr > ($#options)) {
      $ptr = 0;
    }
  }
  #
  # Move up the tree one level. Pop the refstack and path, reset the
  # pointer to the top of the list.
  #
  if ( $key eq "LEFTARROW" ) {
    my $depth = @refstack;
    if ($depth > 1) {
      pop @refstack;
      pop @path;
      @options = keys %{$refstack[$#refstack]};
      $ptr = 0;
    }
  }
  #
  # Go down one level. 
  #
  if ( $key eq "RIGHTARROW" ) {
    push @refstack, \%{@{$refstack[$#refstack]}{$options[$ptr]}};
    push @path, $options[$ptr];
    @options = keys %{$refstack[$#refstack]};
    #
    # If there are no child elements, we are on a leaf. Log the path,
    # back off @refstack and @path, reset @options.
    #
    my $arrsize = @options;
    if ( $arrsize == 0 ) {
      my $logmsg = join "\t", @path;
      print "Selected: $logmsg\n";
      open my $fh, '>>', "logfile.txt";
      my $ts = POSIX::strftime("%Y-%m-%d %H:%M:%S",localtime());
      print $fh "$ts\t$logmsg\n";
      close $fh;
      #
      # back off refstack, path. reset options
      #
      pop @refstack;
      pop @path;
      @options = keys %{$refstack[$#refstack]};
    }
  }
} until ($key eq 'ENTER') ;

