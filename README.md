# Event Logger

## Overview

The idea is to have a dedicated device in the apartment
that can be used to record human-observable events.
For example, when the cat throws up.
Capturing this data will allow us to detect changes in the frequency of these events.

I envision this as a microcontroller-based device with a keypad for data entry,
a small LCD screen to display output,
a block-device to store data and,
optionally (but preferably),
a Wi-Fi connection so that the collected data can be sent elsewhere.

I initially planned to implement this in Lisp on some kind of specialty MCU.
I was looking at the [MakerLisp Lisp Machine](https://makerlisp.com) for a minute.
After learning a lot about Lisp,
I decided that Lisp is maybe too high-level for this kind of project.
It's a bit solipsistic when you start looking at input and output options.

I then looked at Forth.
Forth is perfect for a low-level implementation,
and I might get there, eventually,
but for now there would be a lot of low-level groundwork to do
before I would have anything useful.

So, for now, I've landed on [Perl](http://modernperlbooks.com/books/modern_perl_2016/index.html)
on a Raspberry Pi Zero W with built-in Wi-Fi.
Output will be handled by a 2x16 LCD display.
Input will be handled by a tiny, 5-key USB macropad that I got from Massdrop,
if I recall correctly.
At this point, all I need is buttons for next and select.
A rotory encoder would be a great fit.
I may need a prev/back button in the near future, however.
Originally, I thought I should have a dedicated button for each event that needed to be tracked, 
but I think that's taking the brute force approach too far.
I'd like to get away from the USB keypad and hook some switches up to the IO pins.
Data captured by the device can be sent somewhere using rsync or just scp.

So, I could implement my first version of the event logger using Perl on a Pi Zero,
(using [Steve Bertrand's RPi::WiringPi](https://metacpan.org/pod/RPi%3A%3AWiringPi)).

### WiringPi

2019-07-20

So, lot's of dependencies in the current plan.

RPi::WiringPi wraps a bunch of other APIs
which, in turn, wrap the [WiringPi](http://wiringpi.com/)
library.
I may be able to get away with using only RPi::LCD for now.
It depends on WiringPi::API.

So, install WiringPi,
install RPi::API,
install RPi::LCD.

I should be able to use cpanm to install RPi::LCD and have its dependencies resolved automatically.


## Data Structure

2019-07-20

I spoke with some of the hackers at the Hackers Unite meetup.
They recommended going with a doubly-linked list.
This would allow me to, with only knowledge of the current node,
navigate to a child node or parent node because
each node includes references to its parent and children.
I think my Perl implementation,
which uses a singly-linked list and a reference stack,
is pretty much baked.
I may re-write it later.
I'm actually more excited about building a doubly-linked list in Forth.


2019-earlier

A flat list would be easy, 
but I think to be really useful, 
a tree structure is needed.
[This perlmonks article](https://www.perlmonks.org/?node_id=102631)
explains a way to create a tree
by reading in a file.
I've basically stolen this as-is.
I'm using a stack-based method to track my path through the tree.

In the log file, I'd like to have the path to the leaf node
so that each choice the user made along the way is represented.
The downside is that, if the tree depth changes,
that's going to create log data that clashes, 
format-wise, with existing log data.
A problem for another day, methinks.

The output log will be one line per entry,
each entry will begin with a time stamp.
The remaining fields will be the tree path, delimited.
I use tabs for this.

## Build Log

2019-07-14 19:25 Pi Zero Setup

1. burned raspbian lite image to a 8GB microSD.
2. mounted the sdcard and `touch ssh` to enable ssh
3. create wpa_supplicant.conf
4. add "enable_uart=1" to the end of config.txt
5. move sdcard to PiZero, plug in USB console cable pins
6. plug in USB console cable, run `sudo dmesg` to get device name
7. screen /dev/ttyUSB0 115200
8. login with the defaults (pi/raspberry) and change the default password.
9. sudo apt install cpanminus
10. sudo cpanm Term::RawInput
11. sudo apt update && sudo apt upgrade -y
12. create a user for myself `adduser --shell /bin/bash dave`
13. add my new user to sudoers `sudo usermod -G sudo dave`

References:

https://www.raspberrypi.org/documentation/installation/installing-images/README.md
https://learn.adafruit.com/adafruits-raspberry-pi-lesson-5-using-a-console-cable/overview
https://howtoraspberrypi.com/how-to-raspberry-pi-headless-setup/
https://cdn-images-1.medium.com/max/1600/0*Zpa1YOQcMlvu-Sxs.png

2019-07-06 23:13 
Installing Term::RawInput so that I can capture arrow keys without having to decode them myself.
And Linux is killing cpan because it uses too much memory on my tiny VPS.
The internet suggests installing cpanm because it uses less memory.

```
sudo apt install cpanminus
sudo cpanm inc::Module::Install
sudo cpanm Term::RawInput
```

Now Perl can't find warnings.pm and my program won't compile.
Seems the answer is to install local::lib

```
sudo cpanm local::lib
```

No dice. Perl is looking in /usr/local/lib/x86_64-linux-gnu/perl
but that folder doesn't exist.

find turns up

```
/usr/share/perl/5.24.1/encoding/warnings.pm
/usr/share/perl/5.24.1/warnings.pm
/usr/lib/x86_64-linux-gnu/perl-base/warnings.pm
```

```
$perl -e "print qq(@INC)"
/etc/perl /usr/local/lib/x86_64-linux-gnu/perl/5.24.1 /usr/local/share/perl/5.24.1 /usr/lib/x86_64-linux-gnu/perl5/5.24 /usr/share/perl5 /usr/lib/x86_64-linux-gnu/perl/5.24 /usr/share/perl/5.24 /usr/local/lib/site_perl /usr/lib/x86_64-linux-gnu/perl-base
```

even perl -V fails.
tried reinstalling perl with something like
```
sudo dpkg --purge --force-dependencies perl
sudo apt install perl
```

Still getting the same error message about permissions.
Lets muck with permissions, then.
```
sudo chmod ugo+rx a_bunch_of_paths
```

Seems to have helped!


