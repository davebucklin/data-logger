\ Trying my hand at a Forth version of the tracker app.

\ set a marker in the event that we reload the file
[IFDEF] tracker-fs
  tracker-fs
[ENDIF]
marker tracker-fs

\ navigation constants
\ vi-bindings, q to quit
\ char h constant arrow-left
\ char j constant arrow-down
\ char k constant arrow-up
\ char l constant arrow-right
2147483648 constant arrow-left
2147483651 constant arrow-down
2147483650 constant arrow-up
2147483649 constant arrow-right
char q constant qqq

\ structure and words for navigating options
16 constant elements
create tree elements cells 5 * allot ( five cells per element )
\ { value | parent | child | prev | next }
variable ptr
: reset tree ptr ! ;
reset
\ ptr = address of the ptr
\ ptr @ = beginning address of the node, should always point at a value address
\ ptr @ n cells + = address of the node + n cells
\ ptr @ n cells + @ = value in cell n
\ if the pointer value is zero, it's a NOP
: value! ( addr -- ) ptr @ ! ;
: value@ ( -- addr ) ptr @ @ ;
: <parent> ( -- addr ) ptr @ cell + ;
: parent! ( addr -- ) <parent> ! ;
: >parent ( -- ) <parent> @ ?dup if ptr ! then ;
: <child> ( -- addr ) ptr @ 2 cells + ;
: child! ( addr -- ) <child> ! ;
: >child ( -- ) <child> @ ?dup if ptr ! then ;
: <prev> ( -- addr ) ptr @ 3 cells + ;
: prev! ( addr -- ) <prev> ! ;
: >prev ( -- ) <prev> @ ?dup if ptr ! then ;
: <next> ( -- addr ) ptr @ 4 cells + ;
: next! ( addr -- ) <next> ! ;
: >next ( -- ) <next> @ ?dup if ptr ! then ;
: >forward ( -- ) ptr @ 5 cells + ptr ! ;
: navigate ( key -- )
  dup arrow-right = if >child else
  dup arrow-left = if >parent else
  dup arrow-up = if >prev else
  dup arrow-down = if >next
  then then then then drop ;

\ text label buffer
\ our LCD display is 16 chars wide
16 constant width
width 16 * constant bufsize
create labels bufsize allot
labels bufsize bl fill ( fill with spaces )
\ here, we hunt for an open spot to put a string
: label! ( addr len -- addr )
  width min ( cap len at 16 )
  0 width 0 do ( we have 16 segments to check )
    drop
    labels i width * + dup c@ ( addr len labels-addr c )
    bl = if \ if we land on a space,
      \ move the string there and exit the loop
      dup >r
      swap cmove
      r>
      leave
    else
      drop ( drop the labels-addr )
    then
    0 ( this will be the labels-addr if we don't find a spot )
  loop ;
: .option ( -- ) cr value@ width -trailing type ;

\ populate data structure
s" Abner" label! value!
0 parent!
tree 5 cells + dup next! prev!
tree 10 cells + child!
>forward
s" Arthur" label! value!
0 parent!
tree dup next! prev!
0 child! ( we fix this later ) 
>forward
: midcell ( addr len -- )
label! value!
tree parent!
ptr @ 5 cells + next!
ptr @ 5 cells - prev!
0 child! ;
s" Cough" midcell 0 prev! >forward
s" Hairball" midcell >forward
s" Loose stool" midcell >forward
s" Not eating" midcell >forward
s" Poop outsde box" midcell >forward
s" Sneeze" midcell >forward
s" Vom Food" midcell 0 next! >forward
: midcell ( addr len -- )
label! value!
tree 5 cells + parent!
ptr @ 5 cells + next!
ptr @ 5 cells - prev!
0 child! ;
s" Cough" midcell 0 prev! ptr @ tree 7 cells + ! >forward
s" Hairball" midcell >forward
s" Loose stool" midcell >forward
s" Not eating" midcell >forward
s" Poop outsde box" midcell >forward
s" Sneeze" midcell >forward
s" Vom Food" midcell 0 next!

\ file i/o stolen from the gforth manual
0 Value fd-in
0 Value fd-out
: open-input ( addr u -- )  r/o open-file throw to fd-in ;
: open-output ( addr u -- )  r/w open-file throw to fd-out ;
: close-input ( -- )  fd-in close-file throw ;
: close-output ( -- )  fd-out close-file throw ;

\ logging, uses pad as a staging area
: time-stamp ( -- ) ( builds a timestamp on the pad )
  time&date
  0 <# [char] - hold #s #> pad place
  0 <# [char] - hold # # #> pad +place
  0 <# 32 hold # # #> pad +place
  0 <# [char] : hold # # #> pad +place
  0 <# [char] : hold # # #> pad +place
  0 <# 9 hold # # #> pad +place ;
: write-pad ( -- ) pad count fd-out write-line throw fd-out flush-file throw ;
: >option begin value@ 16 -trailing pad +place <parent> @ while >parent repeat ;
: add-a-tab pad c@ 1+ dup pad c! pad + 09 swap c! ;
: >option 0 begin value@ swap 1+ <parent> @ while >parent repeat ( addr [addr] n ) 0 do 16 -trailing pad +place add-a-tab loop ;
: log ( -- ) time-stamp >option write-pad ;

\ the main event
: tracker ( -- )
  reset
  s" logfile.txt" open-output fd-out file-size throw ( open file )
  fd-out reposition-file throw ( set pointer to the end of the file )
  begin 
    .option
    ekey
    \ when we try to >child on a leaf node, we create a log entry.
    dup arrow-right = <child> @ 0= and if
      ."  Selected" log reset
    else
      dup navigate
    then
  qqq = until
  close-output
  ." bye!"
  quit ;
